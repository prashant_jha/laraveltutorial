<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;

class UserController extends Controller
{
    public function index(){

        //Insert the values in model
        // $user = new User();

        // $user -> name = "Admin";
        // $user -> email = "pjha123@gmail.com";
        // $user -> password = "password";

        // $user -> save();

        //The above 5 lines of code can be reduced to 2 lines as below

        
        // $data = [
        //     'name' => 'Prashant',
        //     'email' => 'pjha34@gmail.com',
        //     'password' => bcrypt('password')
        // ];

        //Use create method to insert the value in model
        // User::create($data);

        // Update the model using eloquent
        // User::where('id', 1)->update(['name'=>'Shubham']);

        //Delete the model record using eloquent
        // User::where('id','2')->delete();


        //See all the results of model using eloquent


         $user_result = User::where('name','Prashant')->get();

         foreach($user_result as $user){
             $user_name = $user -> name;
         }


        return $user_name;
    }
}
