@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

            
                <div class="card-body">

                    <div>
                        <img src="{{asset('/storage/images/'.Auth::user()->avatar)}}" width="200" height="200" style="border-radius:50%; margin:0 auto; display:block;"/>
                    </div>

                    <div class="mt-2" style="text-align:center;">
                        <h5><b>Name:</b> {{Auth::user()->name}}</h5> 
                        <h5><b>Email:</b> {{Auth::user()->email}}</h5> 
                    </div>

                    
                    
                    <!-- <h4>Upload profile picture</h4> -->


                    <!-- <form action="/upload" method="POST" enctype = "multipart/form-data">
                        @csrf
                        <input type="file" accept="image/*" name="image"/>
                        <button class="btn-primary btn" type="submit" name="upload">Upload</button>
        
                    </form> -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
